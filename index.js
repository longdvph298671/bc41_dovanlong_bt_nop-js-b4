
/**
 * bài 1
 * input:
 *  nhập 3 số từ bàn phím
 * 
 * bước xử lý
 * b1: tạo biến so1, so2, so3, tam (biến tạm để đổi chỗ)
 * b2: nếu so1 > so2 thì đổi vị trí nếu ko bỏ qua
 * b3: nếu so1 > so3 thì đổi vị trí nếu ko bỏ qua
 * b4: nếu so2 > so3 thì đổi vị trí nếu ko bỏ qua
 * b5: in kết quả 3 số bằng innerHTML
 * 
 * output:
 *  kết quả danh sách 3 số đã được sắp xếp.
 */
var result = document.getElementById("result");
function sapXep(){
    let so1 = document.getElementById("soNhapVao1").value*1;
    let so2 = document.getElementById("soNhapVao2").value*1;
    let so3 = document.getElementById("soNhapVao3").value*1;
    let tam;
    if(so1 > so2){
        tam = so1;
        so1 = so2;
        so2 = tam;
    }
    if(so1 > so3){
        tam = so1;
        so1 = so3;
        so3 = tam;
    }
    if(so2 > so3){
        tam = so2;
        so2 = so3;
        so3 = tam;
    }
    result.innerHTML = `<h3>${so1}, ${so2}, ${so3}</h3>`;
}
/**
 * bài 2
 * input:
 *  người dùng chon ai đó
 * 
 * bước xử lý
 * b1: tạo biến thanhVien;
 * b2: gán biến thanhVien = value người dùng đã chọn;
 * b2: nếu thanhVien == B thì in Chào Bố! bằng innerHTML
 * b2: nếu ko thì thanhVien == M thì in Chào Mẹ! bằng innerHTML
 * b2: nếu ko thì thanhVien == A thì in Chào Anh Trai! bằng innerHTML
 * b2: nếu ko thì thanhVien == E thì in Chào Em Gái! bằng innerHTML
 * b2: ngược lại thì in Xin Chào Người Lạ Ơi! bằng innerHTML
 * 
 * output:
 *  kết quả XIN CHÀO người mà người dùng đã chọn
 */
function chaoHoi(){
    var thanhVien =document.getElementById("thanhVien").value;
    
    if(thanhVien == "B"){
        result.innerHTML = `<h3>Xin Chào Bố!</h3>`;
    }
    else if(thanhVien == "M"){
        result.innerHTML = `<h3>Xin Chào Mẹ!</h3>`;
    }
    else if(thanhVien == "A"){
        result.innerHTML = `<h3>Xin Chào Anh Trai!</h3>`;
    }
    else if(thanhVien == "E"){
        result.innerHTML = `<h3>Xin Chào Em Gái!</h3>`;
    }
    else{
        result.innerHTML = `<h3>Xin Chào Người Lạ Ơi! ^ ^</h3>`;
    }
}
/**
 * bài 3
 * input:
 * nhập 3 số từ bàn phím
 * 
 * bước xử lý
 * b1: tạo mảng listSo, biến demSoChan, demSoLe
 * b2: dùng vòng lặp for để duyệt mảng
 * b3: nếu lần lượt các phần tử trong mảng listSo chia hết cho 2 thì demSoChan cộng thêm 1
 * b4: tính demSoLe = 3 - demSoChan
 * b5: in kết quả demSoChan và demSoLe bằng innerHTML
 * 
 * output:
 *  kết quả n số chẵn và n số lẻ
 */
function demChanLe(){
    let listSo = document.getElementsByClassName("nhap-So");
    let demSoChan = 0, demSoLe = 0;
    for(i = 0; i < listSo.length; i++){
        if((listSo[i].value*1)%2 == 0){
            demSoChan++;
        }
    }
    demSoLe = 3 - demSoChan;
    result.innerHTML = `<h3>Có ${demSoChan} số chẵn, ${demSoLe} số lẻ</h3>`;
}



/**
 * bài 4
 * input:
 *  nhập 3 số từ bàn phím
 *  
 * bước xử lý
 * b1: tạo biến a, b, c
 * b2: gán giá trị cho a,b,c từ input
 * b3: nếu a + b > c && a + c > b && b + c > a thì đây là tam giác ngược lại ko tạo thành tam giác
 * b4: nếu là tam giác thì:
 *              nếu (a == b && a == c) thì là tam giác đều
 *              nếu ko thì (a == b || a == c || b == c) thì là tam giác cân
 *              nếu ko thì (a*a == b*b + c*c || b*b == a*a + c*c || c*c == a*a + b*b) thì là tam giac vuông
 *              ngược lại là một loại tam giác nào đó!
 * b5: in kết quả có phải là tam giác hay ko hoặc là loại tam giác nào đó bằng innerHTML
 * 
 * output:
 *  kết quả là loại tam giác tương ứng
 */
//bai4
function ktTamGiac(){
    let a = document.getElementById("canh1").value*1;
    let b = document.getElementById("canh2").value*1;
    let c = document.getElementById("canh3").value*1;
    if(a + b > c && a + c > b && b + c > a){
        if(a == b && a == c ){
            result.innerHTML = `<h3>Hình Tam Giác Đều</h3>`;
        }
        else if(a == b || a == c || b == c){
            result.innerHTML = `<h3>Hình Tam Giác Cân</h3>`;
        }
        else if(a*a == b*b + c*c || b*b == a*a + c*c || c*c == a*a + b*b){
            result.innerHTML = `<h3>Hình Tam Giác Vuông</h3>`;
        }
        else {
            result.innerHTML = `<h3>Một Loại Tam Giác Khác</h3>`;
        }
    }
    else{
        alert("Dữ Liệu Không Hợp Lệ!!!")
    }
}



